#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 17 11:21:54 2022

@author: vibhaasenthilkumaravelan
Description:
Longest increasing sequence


"""
try:
    num_list = [10, 9, 2, 5, 3, 7, 101, 18]
    a = 0
    b = 1
    print(num_list)
    while True:
        if num_list[a] < num_list[b]:
            a += 1
            b += 1
        else:
            num_list.pop(a)
        print(num_list)
except IndexError:
        print(len(num_list))